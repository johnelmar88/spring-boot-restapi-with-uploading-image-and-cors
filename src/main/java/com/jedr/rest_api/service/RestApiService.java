package com.jedr.rest_api.service;

import com.jedr.rest_api.entity.RestApiEntity;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

public interface RestApiService {

    public Page<RestApiEntity> getAllData(int page, int size);
    public RestApiEntity getDataById(String id);

    public RestApiEntity insertData(RestApiEntity apiEntity);

    void deleteData(String id);

    public String uploadPhoto(String id, MultipartFile file);


}
