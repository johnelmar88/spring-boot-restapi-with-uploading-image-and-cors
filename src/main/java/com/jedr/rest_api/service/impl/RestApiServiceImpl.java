package com.jedr.rest_api.service.impl;


import com.jedr.rest_api.entity.RestApiEntity;
import com.jedr.rest_api.repo.RestApiRepo;
import com.jedr.rest_api.service.RestApiService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

import static com.jedr.rest_api.config.constant.Constant.PHOTO_DIRECTORY;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

@Service
@Slf4j
@Transactional(rollbackOn = Exception.class)
@RequiredArgsConstructor
public class RestApiServiceImpl implements RestApiService {

    private final RestApiRepo restApiRepo;

    @Override
    public Page<RestApiEntity> getAllData(int page, int size) {
        return restApiRepo.findAll(PageRequest.of(page, size, Sort.by("name")));
    }

    @Override
    public RestApiEntity getDataById(String id) {
        return restApiRepo.findById(id).orElseThrow(() -> new RuntimeException("No Id"));
    }

    @Override
    public RestApiEntity insertData(RestApiEntity apiEntity) {
        return  restApiRepo.save(apiEntity);

    }

    @Override
    public void deleteData(String id) {
//         restApiRepo.delete(id);
    }

    @Override
    public String uploadPhoto(String id, MultipartFile file) {
   log.info("Saving Picture for User Id: {}", id);
   RestApiEntity restApi = getDataById(id);
    String photoUrl = photoFunction.apply(id, file);
    restApi.setPhotoUrl(photoUrl);
    restApiRepo.save(restApi);
    return photoUrl;
    }

    private final Function<String, String> fileExtension = filename -> Optional
            .of(filename)
            .filter(name -> name.contains("."))
            .map(name ->  name.substring(filename.lastIndexOf(".")+1))
            .orElse(".png");


    private final BiFunction<String, MultipartFile, String> photoFunction = (id, image) -> {
        String filename = id + fileExtension.apply(image.getOriginalFilename());
        try{
            Path fileStorageLocation = Paths.get(PHOTO_DIRECTORY).toAbsolutePath().normalize();
            if (!Files.exists(fileStorageLocation)){
                Files.createDirectories(fileStorageLocation);
            }
            Files.copy(image.getInputStream(), fileStorageLocation.resolve(filename), REPLACE_EXISTING);
            return ServletUriComponentsBuilder
                    .fromCurrentContextPath()
                    .path("/restapi/image/" + id + fileExtension.apply(image.getOriginalFilename())).toUriString();
             } catch (Exception exception){
            throw  new RuntimeException("Unable to save image");
        }
    };
}