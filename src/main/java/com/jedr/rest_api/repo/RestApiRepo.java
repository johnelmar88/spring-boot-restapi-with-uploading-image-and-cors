package com.jedr.rest_api.repo;

import com.jedr.rest_api.entity.RestApiEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface RestApiRepo extends JpaRepository<RestApiEntity, String> {
    Optional<RestApiEntity> findById(String id);

//    void delete(String id);
}
