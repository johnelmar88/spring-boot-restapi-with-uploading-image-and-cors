package com.jedr.rest_api.controller;

import com.jedr.rest_api.entity.RestApiEntity;
import com.jedr.rest_api.service.RestApiService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;

import static com.jedr.rest_api.config.constant.Constant.PHOTO_DIRECTORY;
import static org.springframework.http.MediaType.IMAGE_JPEG_VALUE;
import static org.springframework.http.MediaType.IMAGE_PNG_VALUE;

@RestController
@RequestMapping("/restapi")
@RequiredArgsConstructor
public class RestApiController {

    private final RestApiService restApiService;


    @GetMapping
    public ResponseEntity<Page<RestApiEntity>> fetchAllData(@RequestParam(value = "page", defaultValue = "0") int page,
                                                            @RequestParam(value = "size", defaultValue = "10") int size){
return  ResponseEntity.ok().body(restApiService.getAllData(page, size));
    }
    @PostMapping("")
    public ResponseEntity<RestApiEntity> insertData(@RequestBody RestApiEntity restApi) {
        return ResponseEntity.created(URI.create("/restapi/userID")).body(restApiService.insertData(restApi));

    }

    @GetMapping("{id}")
    public ResponseEntity<RestApiEntity> fetchDataById(@PathVariable("id") String id){
        return  ResponseEntity.ok().body(restApiService.getDataById(id));

    }

    @GetMapping("/photo")
    public ResponseEntity<String> uploadPhoto(@RequestParam("id")String id,
                                              @RequestParam("file")MultipartFile file){
        return ResponseEntity.ok().body(restApiService.uploadPhoto(id, file));
    }


    @GetMapping(path = "/image/{filename}", produces = {IMAGE_PNG_VALUE, IMAGE_JPEG_VALUE})
    public byte[] fetchPhoto(@PathVariable("filename")String filename) throws IOException{
        return Files.readAllBytes(Paths.get(PHOTO_DIRECTORY + filename));

    }
}
